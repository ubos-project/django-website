from django.apps import AppConfig


class DataCapabilitiesConfig(AppConfig):
    name = 'Data_Capabilities'
