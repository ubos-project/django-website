from django.shortcuts import render
from django.urls import reverse
from django.template import loader
from django.http import HttpResponse
from django.shortcuts import render, redirect
from .forms import UserCreationForm



def Home(request):
    template = loader.get_template('homePage.html')
    return HttpResponse(template.render())


def Test(request):
    template = loader.get_template('test.html')
    return HttpResponse(template.render())

def new(request):
    temp = loader.get_template('newfile.html')
    return HttpResponse(temp.render())

def signUp(request):
    template = loader.get_template('signUP.html')
    return HttpResponse(template.render())

# Create your views here.

def create_user(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            # You can add any additional logic here, such as sending a confirmation email
            return redirect('user_created')  # Redirect to a success page
    else:
        form = UserCreationForm()
    
    return render(request, 'create_user.html', {'form': form})
