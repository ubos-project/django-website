from django.apps import AppConfig


class DigitalSolutionsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Digital_Solutions'
