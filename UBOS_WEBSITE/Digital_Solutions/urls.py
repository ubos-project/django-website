from .views import Home
from django.urls import path
from . import views


app_name = 'Digital_Solutions'

urlpatterns = [
    path('', views.signUp,name='signUp'),
    path('home', views.Home, name='Home'),
    path('test', views.Test, name='Test'),
    path('new',views.new,name="new"),
    path('create_user',views. create_user,name=" create_user")

]
